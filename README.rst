QRMR, Terminal toolkit for AWS
=============================================

    Work simpler and more secure with with AWS terminal tools like ``aws``-cli, ``aws-shell``, ``terraform``,
    using Multi-Factor Authentication (MFA / 2FA) and AWS security best practices.

QRMR stands for Quartermaster :)

*Terminal toolkit to make using Amazon Web Services (AWS) simpler and more secure (2FA / MFA).*

Written in Python 3, backwards compatible with Python 2, thanks to ``futures``.

Currently being heavily tested in production against AWS multi-account setup (Well-Architected Framework) on macOS High Sierra.

Feels most at home using `virtualenv`s, of course.

**How it works:**

* Stores your AWS IAM credential profile in ``~/.qrmr/credentials.ini``;
* Prompts for MFA OTP code;
* Uses AWS STS to retrieve and store fresh SessionToken and temporary Access Key ID and Secret Access Key using your credential profile.

**Near future:**

* Manage ``~/.aws/credentials`` and ``~/.aws/config`` files
* Unit Tests :)

Because you probably just want to start using it:

**Installation of QRMR:**

First install PIP if you do not already have it:

Download it:

``wget https://bootstrap.pypa.io/get-pip.py``

Install it:

``python get-pip.py``

Use it:

``pip install qrmr``

**Setup of AWS Credentials:**

``qrmr setup``

**Refreshing your SessionToken and temporary keys:**

``qrmr refresh``

**Be cool:**

``aws s3 ls``


**REMEMBER:** set environment variable AWS_PROFILE in your shell or virtualenv to
make life easier:

``export AWS_PROFILE=iam_user_name``

Find out more features by running:

``qrmr --help``

Find us on: https://gitlab.com/qrmr/qrmr

Reminder for QRMR dev's on how to sign:
---------------------------------------

Using keybase:

``keybase pgp sign -d -i dist/qrmr-0.4.2.tar.gz -o dist/qrmr-0.4.2.tar.gz.asc``

Resources:
----------

- https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html


License / Copyright / Disclaimer:
---------------------------------

BSD 3-Clause Revised License

Copyright (c) 2017 - 2018, all rights reserved by QRMR / ALDG / Alexander L. de Goeij.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

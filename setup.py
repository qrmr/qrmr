#!/usr/bin/env python
# -*- coding: utf-8 -*-
#   ____  _____  __  __ _____
#  / __ \|  __ \|  \/  |  __ \
# | |  | | |__) | \  / | |__) |
# | |  | |  _  /| |\/| |  _  /
# | |__| | | \ \| |  | | | \ \
#  \___\_\_|  \_\_|  |_|_|  \_\
#
# Terminal toolkit to make using Amazon Web Services (AWS) simpler and more secure (2FA / MFA).
#
# Find us on: https://gitlab.com/qrmr/qrmr
#
# Copyright (c) 2017 - 2018, all rights reserved by QRMR / ALDG / Alexander L. de Goeij.
# All rights reserved.
#
# BSD 3-Clause Revised License applies. (see LICENSE file).
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
from setuptools import setup, find_packages
from qrmr import __version__


setup(
    name="qrmr",
    version=__version__,
    author="Alexander L. de Goeij",
    author_email="mail@aldg.nl",
    description=("Terminal toolkit to make using Amazon Web Services (AWS) simpler and more secure (2FA / MFA)."),
    license="BSD 3-Clause Revised",
    keywords="aws aws-cli aws-sdk cli terminal mfa 2fa multi-factor-authentication iam-credentials login otp session token",
    url="https://gitlab.com/qrmr/qrmr",
    packages=find_packages(),
    install_requires=['future', 'colorlog',
                      'boto3', 'configparser', 'requests'],
    long_description=open('README.rst').read(),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Environment :: MacOS X",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 2.7",
        "Topic :: Software Development",
        "Topic :: Utilities",
    ],
    entry_points={
        'console_scripts': [
            "qrmr=qrmr.qrmr:main",
        ]
    },
)

.. qrmr documentation master file, created by
   sphinx-quickstart on Wed Nov 22 09:32:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for QRMR:
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

ToDo list for this version:
---------------------------

.. todolist::

.. automodule:: qrmr.qrmr
   :members:
   :undoc-members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

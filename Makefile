TARGETS = html
SUBDIRS = docs

init:
	pip3 install -r requirements.txt

.PHONY: build
build:
	rm -rf dist
	pip3 install -e .
	python3 setup.py sdist bdist_wheel

.PHONY: docs
docs:
	pip3 install -e .
	for target in $(TARGETS); do \
			$(MAKE) -C docs $$target; \
	done

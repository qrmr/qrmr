import colorlog
import boto3
from botocore.exceptions import ClientError
import base64
import qrcode


logger = colorlog.getLogger(__name__)


def create_user(args):
    """Create new IAM User, set or update temporary password and add to IAM Group."""
    logger.debug("iam_create_user called")

    client = boto3.client('iam')

    # Create new IAM User account, or fail gracefully if it exists
    try:
        logger.debug("trying to create_user")
        response = client.create_user(
            Path='/',
            UserName=args.user
        )
        logger.debug(response)
        logger.info("Succesfully created new IAM User '%s'", args.user)

    except client.exceptions.EntityAlreadyExistsException as e:
        logger.warning("User %s already exists.", args.user)
        logger.debug(e)

    # Add temporary password to IAM User account, or update it with a new one
    try:
        logger.debug("trying to create_login_profile")
        response = client.create_login_profile(
            UserName=args.user,
            Password=args.password,
            PasswordResetRequired=True
        )
        logger.debug(response)
        logger.info(
            "Succesfully set new temporary password for IAM User '%s'", args.user)

    except client.exceptions.PasswordPolicyViolationException as e:
        logger.error(
            "Password for user '%s' does not match policies, check in AWS Console.", args.user)
        logger.debug(e)

    except client.exceptions.EntityAlreadyExistsException as e:
        logger.warning(
            "Password (login_profile) already set for User '%s', will attempt to set new temporary password.", args.user)
        logger.debug(e)

        try:
            logger.debug("trying to update_login_profile")
            response = client.update_login_profile(
                UserName=args.user,
                Password=args.password,
                PasswordResetRequired=True
            )
            logger.debug(response)
            logger.info(
                "Succesfully updated temporary password for IAM User '%s'", args.user)

        except client.exceptions.PasswordPolicyViolationException as e:
            logger.error(
                "Password for user '%s' does not match policies, check in AWS Console.", args.user)
            logger.debug(e)

    # Add IAM User to IAM Group
    try:
        logger.debug("trying to add_user_to_group")
        response = client.add_user_to_group(
            GroupName=args.group,
            UserName=args.user
        )
        logger.debug(response)

    except Exception as e:
        logger.error("Could not add user '%s' to group '%s'",
                     args.user, args.group)
        logger.debug(e)

    try:
        logger.debug("trying to create_virtual_mfa_device")
        response = client.create_virtual_mfa_device(
            Path='/',
            VirtualMFADeviceName="vmfa--" + args.user
        )
        logger.debug(response)
        print(response["VirtualMFADevice"]["QRCodePNG"])

        img = qrcode.make(base64.b64decode(
            response["VirtualMFADevice"]["QRCodePNG"]))

        print(img)

    except client.exceptions.EntityAlreadyExistsException as e:
        logger.warning(
            "MFA device with same name '%s' already exists, will leave it alone.", ("vmfa--" + args.user))
        logger.debug(e)

    except Exception as e:
        logger.error(e)
        pass

    logger.debug("iam_create_user finished")
    pass

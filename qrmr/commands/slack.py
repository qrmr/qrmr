import colorlog
import boto3
from botocore.exceptions import ClientError
import base64
import qrcode
import os
import subprocess
from subprocess import check_output

from slackclient import SlackClient


logger = colorlog.getLogger(__name__)


def chat(args):
    cmd_list = str(args.command).split(" ")
    out = check_output(cmd_list, universal_newlines=True)

    logger.debug("STDOUT: %s", out)

    slack_token = os.environ["SLACK_API_TOKEN"]
    sc = SlackClient(slack_token)

    sc.api_call(
        "chat.postMessage",
        channel="%s" % args.to,
        text="QRMR: %s \n%s\n:tada:" % (args.message, out)
    )

    pass

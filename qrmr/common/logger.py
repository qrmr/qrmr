# Python 2.7+ compatibility for e.g. macos
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import sys
import os
import logging
import argparse
import configparser
from configparser import MissingSectionHeaderError
from socket import gaierror
import urllib.request
import json
import pip

from qrmr import __version__
import colorlog
import boto3
from botocore.exceptions import ParamValidationError, ClientError


def logHandler(module_name):
    #logger = logging.getLogger(module_name)
    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter(
        '%(log_color)s%(levelname)s:%(name)s:%(message)s')
    )
    logger = colorlog.getLogger()
    logger.addHandler(handler)
    return logger